<?php
if ($CurrentUser->logged_in() && $CurrentUser->has_priv('autopod')) {
    $this->register_app('autopod', 'AutoPod', 3, 'Automaticaly add podcasts from external source', '1.0');
    $this->require_version('autopod', '2.8.18');
    // $this->add_setting('autopod_vimeo_client_id', 'Vimeo Client Id', 'text', '');
    // $this->add_setting('autopod_vimeo_client_secret', 'Vimeo Client Secret', 'text', '');
    spl_autoload_register(function($class_name){
    	if (strpos($class_name, 'AutoPod')===0) {
    		include(PERCH_PATH.'/addons/apps/autopod/'.$class_name.'.class.php');
    		return true;
    	}
    	return false;
    });
    spl_autoload_register(function($class_name){
    	if (strpos($class_name, 'PerchPodcasts')===0) {
    		include(PERCH_PATH.'/addons/apps/perch_podcasts/'.$class_name.'.class.php');
    		return true;
    	}
    	return false;
    });
}