<?php 

function getAlbums($vAccessToken, $URI, $API){
	include '../inc/vimeo-api/autoload.php';
	$Settings = $API->get('Settings');
	$output = array();
	$vUser = false;
	//Initialize Vimeo API
	$vlib = new \Vimeo\Vimeo(AUTOPOD_VIMEO_CLIENT_IDENTIFIER, AUTOPOD_VIMEO_CLIENT_SECRET);
	$vlib->setToken($vAccessToken);
	$response = $vlib->request($URI . '/albums', array(), 'GET');
	if ($response['status'] == 200){
		foreach ($response['body']['data'] as $album) {
			$output[] = array('label'=>$album['name'], 'value'=>$album['uri']);
		}
	}
	return $output;
}