<?php
function autopod_get_vimeo_video_id($url)
{
	return preg_split('/\./', pathinfo($url)['filename'])[0];
}

function autopod_vimeo_episode($showSlug, $slug_or_number, $opts=array(), $return=false)
	{
		$showSlug = rtrim($showSlug, '/');
		$slug_or_number = rtrim($slug_or_number, '/');

        $API  = new PerchAPI(1.0, 'perch_podcasts');   

        $defaults = array(
        	'template' => 'vimeo_episode.html',
        	'skip-template' => false,
       	);

        if (is_array($opts)) {
            $opts = array_merge($defaults, $opts);
        }else{
            $opts = $defaults;
        }

        $Shows = new PerchPodcasts_Shows($API);
        $Show  = $Shows->find_by_slug($showSlug);

        if (is_object($Show)) {
        	PerchSystem::set_vars($Show->to_array());

        	$Episodes = new PerchPodcasts_Episodes($API);

        	if (is_numeric($slug_or_number)) {
        		$Episode = $Episodes->find_by_number($Show->id(), (int)$slug_or_number);
        	}else{
        		$Episode = $Episodes->find_by_slug($Show->id(), $slug_or_number);
        	}

        	if (!$opts['skip-template']) {
	        	$Template = $API->get("Template");
		    	$Template->set('podcasts/'.$opts['template'], 'podcasts');
		    }

	        if (is_object($Episode)) {
	        	PerchSystem::set_var('autopod-vimeo-id', autopod_get_vimeo_video_id($Episode->episodeFile()));
				if ($opts['skip-template']) {
					return $Episode->to_array();
				}		

		        $html = $Template->render($Episode);
		        
	        }else{
	        	$Template->use_noresults();
		    	$html = $Template->render(array());
	        }

		    if ($return) return $html;
		    echo $html;

	        return false;

        }

        return false;

	}