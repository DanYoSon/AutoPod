AutoPod
=
This is an app for Perch CMS that extends the native podcast app, by adding the ability to automaticaly add vimeo videos from a vimeo pro account to generate a remote podcast.

Installation
-
1. Copy autopod app folder to the `perch/addons/apps` folder
2. Add `autopod` to the apps array in `perch/config/apps.php`
3. Navigate to the AutoPod app in the Perch admin interface, the app page should give a url. Copy this it is needed when registering an app in vimeo.
4. Go to Vimeo's developer portal and create a new app [here](https://developer.vimeo.com/apps).
5. Fill in the required fields.
6. Paste the URL copied in step 3 into the "App Callback URLs" field and click "Create app"
7. In `perch/config/config.php` append the following lines replacing `{{ClientIdentifier}}` and `{{ClientSecret}}` with the correct information from the authenticaton tab of the vimeo app's properties
  +`define('AUTOPOD_VIMEO_CLIENT_IDENTIFIER', '{{ClientIdentifier}}');`
  +`define('AUTOPOD_VIMEO_CLIENT_SECRET', '{{ClientSecret}}');`
8. Make sure that the Perch scheduled tasks are setup and working.

Adding a User
-
In order to index a vimeo account for files a pro user account is required, the following steps adds this user to AutoPod
1. Click "Vimeo Users"
2. Click "Add User"
3. Click "Authorize"
4. You will be redirected to Vimeo to authorize AutoPod, you will be redirected to your perch admin interface when the process is complete.
5. You should see the name and bio of the Vimeo user you just authorized

Adding a Show
-
1. From the AutoPod admin home page click add show.
2. Select the show that the videos will be added to and click next.
3. Select the user and the album that will be indexed
When the scheduled tasks run next the videos will be added automaticaly.