<?php
    # include the API
    include('../../../core/inc/api.php');
    include 'inc/appcheck.php';

    $API  = new PerchAPI(1.0, 'autopod');
    $Lang = $API->get('Lang');

    # Set the page title
    $Perch->page_title = $Lang->get('Manage Automatic Shows');

    include('modes/show.list.pre.php');
    
    # Top layout
    include(PERCH_CORE . '/inc/top.php');

    
    # Display your page
    include('modes/show.list.post.php');
    
    
    # Bottom layout
    include(PERCH_CORE . '/inc/btm.php');