<?php 
include '../vimeo/getAlbums.php';
$HTML = $API->get('HTML');
$Lang = $API->get('Lang');
$Form = $API->get('Form');

$aShowFactory = new AutoPod_Shows($API);
$messages = array();

$Perch->add_javascript($API->app_path() . '/js/vimeoalbums.js');

if (isset($_GET['id']) && $_GET['id'] != '') {
	$aShow = $aShowFactory->find($_GET['id']);
	$details = $aShow->to_array();
} else {
	$aShow = false;
	$details = array();
	$aShowType = false;
}

if ($Form->submitted()){
	$postvars = array('showID', 'aShowType');
	$data = $Form->receive($postvars);

	//If type is vimeo get data
	if ($details['aShowType'] == 'vimeo') {
		$postvars = array('vUserID', 'vShowAlbum');
		$vimeoData = $Form->receive($postvars);
		$details['showDynamicFields'] = PerchUtil::json_safe_encode($vimeoData);
	}

	if (is_object($aShow)) {
		$aShow->update($details);
		$messages[] = array('level' => 'success', 'message' => $Lang->get('Show %s successfully updated', $aShow->showTitle));
		$details = $aShow->to_array();
		unset($_GET['created']);
	} else {
		$aShow = $aShowFactory->create($data);
		PerchUtil::redirect($API->app_path() .'/edit/?id='.$aShow->id().'&created=1');
	}
	
}
if (isset($_GET['created'])) {
	$messages[] = array('level' => 'success', 'message' => $Lang->get('Show %s successfully created', $aShow->showTitle));

}
$Form->set_defaults($details);