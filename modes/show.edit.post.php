<?php

echo $HTML->side_panel_start();

echo $HTML->side_panel_end();

echo $HTML->main_panel_start();
include('_subnav.php');
echo $HTML->heading1('Add/Edit Show');
foreach ($messages as $message) {
	$Alert->set($message['level'], $message['message']);
	$Alert->Output();
}
echo $Form->form_start();
if (!array_key_exists('aShowType', $details)) {
	echo $HTML->heading2('Config');
	$typeOpts = array();
	$typeOpts[] = array('label' => 'Vimeo', 'value' => 'vimeo');
	echo $Form->select_field('aShowType', 'Type', $typeOpts);
	$showFactory = new PerchPodcasts_Shows();
	$Shows = $showFactory->all();
	$showOpts = array();
	foreach ($Shows as $show) {
		$showOpts[] = array('label'=>$show->showTitle(), 'value'=>$show->id());
	}
	echo $Form->select_field('showID', 'Show', $showOpts);
} elseif (array_key_exists('aShowType', $details) && $details['aShowType'] == 'vimeo') {
	$vUsersFactory = new AutoPod_VimeoUsers($API);
	$userlist = array();
	$userlist[] = array('label'=>'Select', 'value'=>'');
	$vUsers = $vUsersFactory->all();
	if (is_array($vUsers)) {
		foreach ($vUsers as $vUser) {
			$userlist[] = array('label'=>$vUser->vimeoName(), 'value'=>$vUser->id());
		}
	}
	echo $Form->select_field('vUserID', 'Vimeo User', $userlist);
	$albumlist = array();
	if (isset($details['vShowAlbum']) && $details['vShowAlbum'] != '') {
		$vUser = $vUsersFactory->find($details['vUserID']);
		$albumlist = getAlbums($vUser->vAccessToken(), $vUser->vimeoURI(), $API);
	}
	echo $Form->select_field('vShowAlbum', 'Album', $albumlist);
}

echo $Form->submit_field('btnSubmit', 'Save', $API->app_path(). '/edit/');
echo $Form->form_end();


echo $HTML->main_panel_end();