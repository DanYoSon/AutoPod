<?php 
$HTML = $API->get('HTML');
$Lang = $API->get('Lang');

$aShowFactory = new AutoPod_Shows($API);

$aShows = $aShowFactory->all();

if (!$aShows){
	$aShowFactory->attempt_install();
}