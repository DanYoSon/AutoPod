<?php
$HTML = $API->get('HTML');
$Lang = $API->get('Lang');
$Form = $API->get('Form');
$messages = array();
$vUsersFactory = new AutoPod_VimeoUsers($API);
$deleted = false;
if (!$CurrentUser->has_priv('autopod.vimeo.user.delete')) {
	PerchUtil::redirect($API->app_path());
}

if (isset($_GET['id']) && $_GET['id'] != '') {
	$vUser = $vUsersFactory->find($_GET['id']);
} else {
	$vUser = false;
}

if (is_object($vUser)) {
	$details = $vUser->to_array();
} else {
	$details = false;
}

$Form->set_name('delete');

if ($Form->submitted()) {
	if (is_object($vUser)) {
	    $vUser->delete();
	    if ($Form->submitted_via_ajax) {
	        echo $API->app_path().'/vimeo/user';
	        exit;
	    }else{
	       $deleted = true;
	    }
    }else{
        
    }
}