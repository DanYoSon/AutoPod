<?php
echo $HTML->side_panel_start();

echo $HTML->side_panel_end();

echo $HTML->main_panel_start();
include('_subnav.php');
echo $HTML->heading1('Delete User');
if ($deleted) {
	echo $HTML->success_message('Delete %s deleted successfully', $details['vimeoName']);
} elseif (is_array($details)) {
	echo $HTML->warning_message('Are you sure you wish to delete the user %s?', $details['vimeoName']);
    echo $Form->form_start();
	echo $Form->submit_field('btnSubmit', 'Delete', $API->app_path());
    echo $Form->form_end();
} else {
	echo $HTML->failure_message('No user found');
}
echo $HTML->main_panel_end();