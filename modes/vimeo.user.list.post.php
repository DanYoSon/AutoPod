<?php

echo $HTML->side_panel_start();

echo $HTML->side_panel_end();

echo $HTML->main_panel_start();
include('_subnav.php');

if ($CurrentUser->has_priv('autopod.vimeo.users.create')) echo '<a class="add button" href="'.$HTML->encode($API->app_path().'/vimeo/user/edit/').'">'.$Lang->get('Add User').'</a>';
echo $HTML->heading1('User List');

if (PerchUtil::count($vUsers)) { ?>
	<ul class="smartbar">
        <li class="selected"><a href="<?php echo PerchUtil::html($API->app_path()); ?>"><?php echo $Lang->get('All'); ?></a></li>
    </ul>
<?php } else {
    $Alert->set('notice', $Lang->get('There are no users yet.'));
}
$Alert->Output();

if (PerchUtil::count($vUsers)){ ?>
	<table>
			<thead>
			<tr>
				<td class="first"><?php echo $Lang->get('Name'); ?></td>
				<td><?php echo $Lang->get('Link'); ?></td>
				<td class="action last"></td>
			</tr>
			</thead>
			<tbody>
<?php
	foreach ($vUsers as $vUser) { 
		?>
				<tr>
					<td><a href="<?php echo $HTML->encode($API->app_path()); ?>/vimeo/user/edit/?id=<?php echo $HTML->encode(urlencode($vUser->id())); ?>"><?php echo $vUser->vimeoName(); ?></a></td>
					<td><a href="<?php echo $HTML->encode($vUser->vimeoLink()); ?>" target="blank">Vimeo Profile</a></td>
					<td><?php if ($CurrentUser->has_priv('autopod.vimeo.users.delete')) { ?><a href="<?php echo $HTML->encode($API->app_path()); ?>/vimeo/user/delete/?id=<?php echo $HTML->encode(urlencode($vUser->id())); ?>" class="delete inline-delete" data-msg="<?php echo $Lang->get('Delete this User?'); ?>"><?php echo $Lang->get('Delete'); ?></a><?php } ?></td>
				</tr>
<?php } ?>
			</tbody>
		</table>
<?php
}
echo $HTML->main_panel_end();