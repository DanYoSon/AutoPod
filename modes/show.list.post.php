<?php

echo $HTML->side_panel_start();

echo $HTML->side_panel_end();

echo $HTML->main_panel_start();
include('_subnav.php');

if ($CurrentUser->has_priv('autopod.shows.create')) echo '<a class="add button" href="'.$HTML->encode($API->app_path().'/edit/').'">'.$Lang->get('Add Show').'</a>';
echo $HTML->heading1('Listing Shows');
if (!appcheck($Perch->get_apps(), 'perch_podcasts')) {
	$Alert->set('error', $Lang->get('This app is designed to work with Perch Podcasts, it appears that it is not installed.'));
	$Alert->Output();
}
//Check if API is configured
if (!defined('AUTOPOD_VIMEO_CLIENT_IDENTIFIER') || !defined('AUTOPOD_VIMEO_CLIENT_SECRET')) {
	$redirect_uri = (isset($_SERVER['HTTPS']))?'https://':'http://' . $_SERVER['HTTP_HOST'] . $API->app_path() . '/vimeo/user/edit';
	echo 'Vimeo API not configured<br>';
	echo 'Redirect URI: ' . $redirect_uri;
}
if (PerchUtil::count($aShows)) { ?>
	<ul class="smartbar">
        <li class="selected"><a href="<?php echo PerchUtil::html($API->app_path()); ?>"><?php echo $Lang->get('All'); ?></a></li>
    </ul>
<?php } else {
    $Alert->set('notice', $Lang->get('There are no shows yet.'));
}
$Alert->Output();

if (PerchUtil::count($aShows)){ ?>
	<table>
			<thead>
			<tr>
				<td class="first"><?php echo $Lang->get('Show'); ?></td>
				<td><?php echo $Lang->get('Slug'); ?></td>
				<td class="action last"></td>
			</tr>
			</thead>
			<tbody>
<?php
	foreach ($aShows as $aShow) { 
		?>
				<tr>
					<td><a href="<?php echo $HTML->encode($API->app_path()); ?>/edit/?id=<?php echo $HTML->encode(urlencode($aShow->id())); ?>"><?php echo $aShow->showTitle; ?></a></td>
					<td><?php echo $aShow->showSlug; ?></td>
					<td><a href="<?php echo $HTML->encode($API->app_path()); ?>/delete/?id=<?php echo $HTML->encode(urlencode($aShow->id())); ?>" class="delete inline-delete" data-msg="<?php echo $Lang->get('Delete this Auto Show?'); ?>"><?php echo $Lang->get('Delete'); ?></a></td>
				</tr>
<?php } ?>
			</tbody>
		</table>
<?php
}
echo $HTML->main_panel_end();