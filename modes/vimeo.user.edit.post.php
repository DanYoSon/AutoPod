<?php
function authButton(){
	global $state;
	global $vlib;
	global $redirect_uri;
	$_SESSION['autopod_vimeo_state'] = $state;
	$url = $vlib->buildAuthorizationEndpoint($redirect_uri, array('public'), $state);
	return sprintf('<a href="%s" class="button">Authorise</a>', $url);
}

echo $HTML->side_panel_start();

echo $HTML->side_panel_end();

echo $HTML->main_panel_start();
include('_subnav.php');

echo $HTML->heading1('Edit/Add User');
foreach ($messages as $message) {
	$Alert->set($message['level'], $message['message']);
	$Alert->Output();
}

//Check if API is configured
if (!defined('AUTOPOD_VIMEO_CLIENT_IDENTIFIER') || !defined('AUTOPOD_VIMEO_CLIENT_SECRET')) {
	echo 'Vimeo API not configured<br>';
	echo 'Redirect URI: ' . $redirect_uri;
}
//If no user ask for authentication
elseif (!$VimeoUser) {
	echo authButton();
}
//If user make sure authorization is still valid
elseif ($VimeoUser) {
	echo "<p>";
	echo "<strong>" . $Lang->get("Name:") . "</strong> " . $userDetails['vimeoName'] . "</br>";
	echo "<strong>" . $Lang->get("Bio:") . "</strong> " . $userDetails['vimeoBio'] . "</br>";
	echo "</p>";
	if ($reauth) {
		echo authButton();
	}
}
echo $HTML->main_panel_end();