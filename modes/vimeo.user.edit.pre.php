<?php
//Vimeo Library and variables
include dirname(__FILE__) . '/../inc/vimeo-api/autoload.php';
$state = rand(100, 1000);
$redirect_uri = (isset($_SERVER['HTTPS']))?'https://':'http://' . $_SERVER['HTTP_HOST'] . $API->app_path() . '/vimeo/user/edit';

//Perch helpers
$HTML = $API->get('HTML');
$Lang = $API->get('Lang');
$Form = $API->get('Form');
$Settings = $API->get('Settings');

//Initialize Vimeo API
$vlib = new \Vimeo\Vimeo(AUTOPOD_VIMEO_CLIENT_IDENTIFIER, AUTOPOD_VIMEO_CLIENT_SECRET);

$messages = array();
$reauth = false;

$vUsersFactory = new AutoPod_VimeoUsers($API);
if (!$CurrentUser->has_priv('autopod.vimeo.user.add')) {
	PerchUtil::redirect($API->app_path());
}

if (isset($_GET['id']) && $_GET['id'] != '') {
	$VimeoUser = $vUsersFactory->find($_GET['id']);
	$userDetails = $VimeoUser->to_array();
} else {
	$VimeoUser = false;
	$userDetails = array();
}

if (isset($_GET['code']) && $_GET['code'] != '') {
	PerchUtil::debug($_SESSION);
	if (isset($_SESSION['autopod_vimeo_state']) && $_GET['state'] == $_SESSION['autopod_vimeo_state']) {
		$token = $vlib->accessToken($_GET['code'], $redirect_uri);
		//TODO: add some error checking here
		$userDetails['vAccessToken'] = $token['body']['access_token'];
		unset($_SESSION['autopod_vimeo_state']);
		$messages[] = array(
			'level' => 'success',
			'message' => $Lang->get('Access token retrieved sucessfully')
			);
	} else {
	$messages[] = array(
		'level' => 'error',
		'message' => $Lang->get('State did not Match')
		);
	}
}

if (array_key_exists('vAccessToken', $userDetails)) {
	$vlib->setToken($userDetails['vAccessToken']);
	$response = $vlib->request('/me', array(), 'GET');
	PerchUtil::debug($response);
	if ($response['status'] == 200) {
		PerchUtil::debug('Success');
		$data = array();
		$data['vAccessToken'] = $userDetails['vAccessToken'];
		$data['vimeoName'] = $response['body']['name'];
		$data['vimeoLink'] = $response['body']['link'];
		$data['vimeoBio'] = $response['body']['bio'];
		$data['vimeoURI'] = $response['body']['uri'];
		//Handle reauthentication
		if (!$VimeoUser) {
			$VimeoUser = $vUsersFactory->get_one_by('vimeoURI', $data['vimeoURI']);
		}
		if (!$VimeoUser) {
			$VimeoUser = $vUsersFactory->create($data);
			$messages[] = array(
				'level' => 'success',
				'message' => $Lang->get('User created sucessfully')
				);
		} else {
			$VimeoUser->update($data);
			$messages[] = array(
				'level' => 'success',
				'message' => $Lang->get('User updated sucessfully')
				);
		}
	} else {
		$reauth = true;
		$messages[] = array(
			'level' => 'error',
			'message' => $Lang->get('Authenticaton Error')
			);
	}
}

if ($VimeoUser) {
	$userDetails = $VimeoUser->to_array();
}