<?php
echo $HTML->side_panel_start();

echo $HTML->side_panel_end();

echo $HTML->main_panel_start();
include('_subnav.php');
echo $HTML->heading1('Delete Show');
if ($deleted) {
	echo $HTML->success_message('Show %s deleted successfully', $details['showTitle']);
} elseif (is_array($details)) {
	echo $HTML->warning_message('Are you sure you wish to delete the show %s?', $details['showTitle']);
    echo $Form->form_start();
	echo $Form->submit_field('btnSubmit', 'Delete', $API->app_path());
    echo $Form->form_end();
} else {
	echo $HTML->failure_message('No show found');
}
echo $HTML->main_panel_end();