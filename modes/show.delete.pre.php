<?php
$HTML = $API->get('HTML');
$Lang = $API->get('Lang');
$Form = $API->get('Form');
$messages = array();
$aShowsFactory = new AutoPod_Shows($API);
$deleted = false;
if (!$CurrentUser->has_priv('autopod.shows.delete')) {
	PerchUtil::redirect($API->app_path());
}

if (isset($_GET['id']) && $_GET['id'] != '') {
	$aShow = $aShowsFactory->find($_GET['id']);
} else {
	$aShow = false;
}

if (is_object($aShow)) {
	$details = $aShow->to_array();
} else {
	$details = false;
}

$Form->set_name('delete');

if ($Form->submitted()) {
	if (is_object($aShow)) {
	    $aShow->delete();
	    if ($Form->submitted_via_ajax) {
	        echo $API->app_path().'/';
	        exit;
	    }else{
	       $deleted = true;
	    }
    }else{
        
    }
}