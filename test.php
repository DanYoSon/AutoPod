<?php 
include('../../../core/inc/api.php');
$API  = new PerchAPI(1.0, 'perch_podcasts');
$Template = $API->get('Template');

$Template->set('podcasts/episode.html', 'podcasts');
$tag = $Template->find_all_tags_and_repeaters()[3];
if ($tag->textile) {
	$flang = 'textile';
} elseif ($tag->markdown) {
	$flang = 'markdown';
}
$data = array(
	'summary'=>array(
		'_flang'=>$flang,
		'raw'=>'',
		'processed'=>''));
var_dump(PerchUtil::json_safe_encode($data));
var_dump($tag);

$date = new DateTime('2016-01-15T14:45:54+00:00');
echo $date->format('Y-m-d H:i:s');

$default_url = 'http://home.danielidzerda.ca/podcasts/play/{showSlug}/{episodeNumber}.mp3';

preg_replace_callback('/{([A-Za-z0-9_\-]+)}/', function($matches){
                if ($matches[1] == 'showSlug') {
                	return 'show';
                }
                if ($matches[1] == 'episodeNumber') {
                	return 'title';
                }
            }, $default_url);

$url = 'http://player.vimeo.com/external/120764634.sd.mp4?s=43415fc0f7a7e1d77c5641e323d3b20c73746108&profile_id=112&oauth2_token_id=404239789';
$urlparts = pathinfo($url);
$final = preg_split('/\./', $urlparts['filename']);
var_dump($final[0]);
include 'runtime.php';
$test1 = "Date - Title";
$test2 = "Title";
var_dump(preg_split('/- /', $test1, 2));
var_dump(preg_split('/- /', $test2, 2));
echo sizeof(preg_split('/- /', $test1, 2));