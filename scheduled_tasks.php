<?php
PerchScheduledTasks::register_task('autopod', 'autopod_vimeo_add', 1, 'autopod_vimeo_sched_add');
function autopod_vimeo_sched_add($last_run){
	//Include AutoPod classes (for some reason the Perch Podcasts classes load automagicaly)
	include 'inc/vimeo-api/autoload.php';
	include 'AutoPod_Shows.class.php';
	include 'AutoPod_Show.class.php';
	include 'AutoPod_VimeoUsers.class.php';
	include 'AutoPod_VimeoUser.class.php';
	include 'AutoPod_VimeoEpisodes.class.php';
	include 'AutoPod_VimeoEpisode.class.php';
	//Set up API's
	$API  = new PerchAPI(1.0, 'autopod');
	$Settings = $API->get('Settings');
	$vlib = new \Vimeo\Vimeo(AUTOPOD_VIMEO_CLIENT_IDENTIFIER, AUTOPOD_VIMEO_CLIENT_SECRET);
	//Get Factories
	$aShowFactory = new AutoPod_Shows($API);
	$vUserFactory = new AutoPod_VimeoUsers($API);
	$vEpisodeFactory = new AutoPod_VimeoEpisodes($API);
	//$pShowFactory = new PerchPodcasts_Shows($API);
	$pEpisodeFactory = new PerchPodcasts_Episodes($API);
	//Now GO!!
	$addedvids = 0;
	$badvids = 0;
	$aShows = $aShowFactory->all();
	if (is_array($aShows)) {
		foreach ($aShows as $aShow) {
			$aShowDetails = $aShow->to_array();
			if ($aShowDetails['aShowType'] == 'vimeo') {
				if (array_key_exists('vUserID', $aShowDetails) && array_key_exists('vShowAlbum', $aShowDetails)){
					$vUser = $vUserFactory->find($aShowDetails['vUserID']);
					$vlib->setToken($vUser->vAccessToken());
					$uri = $aShowDetails['vShowAlbum'] . '/videos?sort=date&direction=asc';
					while (true) {
						echo 'Getting: ' . $uri;
						$videos = $vlib->request($uri, array(), 'GET');
						if ($videos['status'] == 200) {
							foreach ($videos['body']['data'] as $video) {
								$vEpisode = $vEpisodeFactory->get_one_by('vimeoURI', $video['uri']);
								if (!is_object($vEpisode)){
									$newep = $vEpisodeFactory->createVimeo($video, $aShow->showID());
									if (is_object($newep)) {
										$addedvids++;
									} else {
										$badvids++;
									}
									
								}
								$vEpisode=null;
							}
						} else {
							return array(
								'result'=>'FAILED',
								'message'=>'Vimeo API Issue');
						}
						if (isset($videos['body']['paging']['next'])) {
							$uri = $videos['body']['paging']['next'];
						} else {
							break;
						}
					}
				}
			}
		}
		PerchUtil::output_debug();
		return array(
			'result'=>'OK',
			'message'=>sprintf('Added %s Videos, Could not add %s videos', (int)$addedvids, (int)$badvids));
	}
	return array(
		'result'=>'OK',
		'message'=>'No Shows to add videos for.');

}
