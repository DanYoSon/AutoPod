<?php

/**
* Vimeo Episode
*/
class AutoPod_VimeoEpisode extends PerchAPI_Base
{
	protected $table = 'autopod_vimeo_episodes';
	protected $pk = 'vEpisodeID';
}