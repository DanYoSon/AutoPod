<?php

/**
* Show Table Row
*/
class AutoPod_Show extends PerchAPI_Base
{
	protected $table = 'autopod_shows';
	protected $pk = 'aShowID';

	public $showTitle = '';
	public $showSlug = '';
	
	public function __Construct($details)
	{
		parent::__Construct($details);

		$showsFactory = new PerchPodcasts_Shows();
		$Show = $showsFactory->find($this->details['showID']);
		$this->showTitle = $Show->showTitle();
		$this->showSlug = $Show->showSlug();
		
	}

	public function to_array()
    {
        $out = parent::to_array();
              
        if ($out['showDynamicFields'] != '') {
            $dynamic_fields = PerchUtil::json_safe_decode($out['showDynamicFields'], true);
            if (PerchUtil::count($dynamic_fields)) {
                foreach($dynamic_fields as $key=>$value) {
                    $out[$key] = $value;
                }
            }
            $out = array_merge($dynamic_fields, $out);
        }
        $out['showTitle'] = $this->showTitle;
        $out['showSlug'] = $this->showSlug;
        return $out;
    }

    public function update($data)
    {
    	if (array_key_exists('showTitle', $data)) {
    		unset($data['showTitle']);
    	}
    	if (array_key_exists('showSlug', $data)) {
    		unset($data['showSlug']);
    	}
        foreach ($data as $key => $value) {
            if (preg_match('/^v/', $key)){
                unset($data[$key]);
            }
        } 
        PerchUtil::debug($data);
    	return parent::update($data);
    }
    
}