<?php
   // Prevent running directly:
    if (!defined('PERCH_DB_PREFIX')) exit;

    // Let's go
    $sql = "
    CREATE TABLE IF NOT EXISTS `__PREFIX__autopod_shows` (
        `aShowID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `showID` int(10) unsigned NOT NULL,
        `aShowType` varchar(255) NOT NULL DEFAULT '',
        `showDynamicFields` longtext DEFAULT '',
        PRIMARY KEY (`aShowID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	CREATE TABLE IF NOT EXISTS `__PREFIX__autopod_vimeo_users` (
        `vUserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `vAccessToken` varchar(255) NOT NULL DEFAULT '',
        `vimeoName` varchar(255) DEFAULT '',
        `vimeoBio` text DEFAULT '',
        `vimeoLink` varchar(255) DEFAULT '',
        `vimeoURI` varchar(255) DEFAULT '',
        PRIMARY KEY (`vUserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    CREATE TABLE IF NOT EXISTS `__PREFIX__autopod_vimeo_episodes` (
        `vEpisodeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
        `episodeID` int(10) unsigned NOT NULL,
        `vimeoURI` varchar(255) DEFAULT '',
        PRIMARY KEY (`vEpisodeID`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	";
    
    $sql = str_replace('__PREFIX__', PERCH_DB_PREFIX, $sql);
    
    $statements = explode(';', $sql);
    foreach($statements as $statement) {
        $statement = trim($statement);
        if ($statement!='') $this->db->execute($statement);
    }

	$API = new PerchAPI(1.0, 'autopod');
    $UserPrivileges = $API->get('UserPrivileges');
    $UserPrivileges->create_privilege('autopod', 'Access AutoPod');
    $UserPrivileges->create_privilege('autopod.shows.create', 'Create New Auto Shows');
    $UserPrivileges->create_privilege('autopod.shows.delete', 'Delete Auto Shows');
    $UserPrivileges->create_privilege('autopod.vimeo.user.add', 'Add a Vimeo User');
    $UserPrivileges->create_privilege('autopod.vimeo.user.delete', 'Delete a Vimeo User');

    $sql = 'SHOW TABLES LIKE "'.$this->table.'"';
    $result = $this->db->get_value($sql);
    
    return $result;