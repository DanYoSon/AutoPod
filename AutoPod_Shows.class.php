<?php

/**
* Factory
*/
class AutoPod_Shows extends PerchAPI_Factory
{
	protected $table = 'autopod_shows';
	protected $pk = 'aShowID';
	protected $singular_classname = 'AutoPod_Show';
}