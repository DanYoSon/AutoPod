<?php 
include('../../../../core/inc/api.php');
include '../vimeo/getAlbums.php';
$API  = new PerchAPI(1.0, 'autopod');
$vUsersFactory = new AutoPod_VimeoUsers($API);
$vUser = false;
if (isset($_GET['vUserID']) && $_GET['vUserID'] != '') {
	$vUser = $vUsersFactory->find($_GET['vUserID']);
}
if (is_object($vUser)) {
	echo PerchUtil::json_safe_encode(getAlbums($vUser->vAccessToken(), $vUser->vimeoURI(), $API));
}