<?php
    # include the API
    include('../../../../core/inc/api.php');
    
    $API  = new PerchAPI(1.0, 'autopod');
    $Lang = $API->get('Lang');

    # Set the page title
    $Perch->page_title = $Lang->get('Delete Automatic Show');

    # Do anything you want to do before output is started
    include('../modes/show.delete.pre.php');
    
    # Top layout
    include(PERCH_CORE . '/inc/top.php');

    
    # Display your page
    include('../modes/show.delete.post.php');
    
    
    # Bottom layout
    include(PERCH_CORE . '/inc/btm.php');