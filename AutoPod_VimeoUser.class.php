<?php

/**
* Vimeo User
*/
class AutoPod_VimeoUser extends PerchAPI_Base
{
	protected $table = 'autopod_vimeo_users';
	protected $pk = 'vUserID';
}