<?php
/**
* Factory
*/
class AutoPod_VimeoUsers extends PerchAPI_Factory
{
	protected $table = 'autopod_vimeo_users';
	protected $pk = 'vUserID';
	protected $singular_classname = 'AutoPod_VimeoUser';
}