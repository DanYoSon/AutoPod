<?php
/**
* Factory
*/
class AutoPod_VimeoEpisodes extends PerchAPI_Factory
{
	protected $table = 'autopod_vimeo_episodes';
	protected $pk = 'vEpisodeID';
	protected $singular_classname = 'AutoPod_VimeoEpisode';

	public function createVimeo($vData, $showID)
	{
		//get api for perch_podcasts to retrive proper template
		$pAPI = new PerchAPI(1.0, 'perch_podcasts');
		$Template = $this->api->get('Template');

		$Template->set('podcasts/episode.html', 'podcasts');
		foreach ($Template->find_all_tags_and_repeaters() as $tag) {
			if ($tag->id == 'summary') {
				if ($tag->textile) {
					$flang = 'textile';
				} elseif ($tag->markdown) {
					$flang = 'markdown';
				}
			}
		}
		$pShow = false;
		$pShowFactory = new PerchPodcasts_Shows($this->api);
		$pShow = $pShowFactory->find($showID);
		$pEpisodeFactory = new PerchPodcasts_Episodes($this->api);
		$data = array();
		$data['showID'] = $showID;
		$data['episodeNumber'] = $pEpisodeFactory->get_next_episode_number_for_show($showID);
		$splitTitle = preg_split('/- /', $vData['name'], 2);
		if (sizeof($splitTitle) == 2) {
			$title = $splitTitle[1];
		} else {
			$title = $splitTitle[0];
		}
		$data['episodeTitle'] = $title;
		$data['episodeSlug'] = PerchUtil::urlify($data['episodeTitle']);
		$date = new DateTime($vData['created_time']);
		$data['episodeDate'] = $date->format('Y-m-d H:i:s');
		$data['episodeDuration'] = $vData['duration'];
		//Currently getting 720p
		$data['episodeFile'] = $vData['files'][1]['link'];
		$data['episodeFileSize'] = $vData['files'][1]['size'];
		$data['episodeFileType'] = $vData['files'][1]['type'];
		//Build JSON for summary field TODO: will need to be more robust some day
		$data['episodeDynamicFields'] = PerchUtil::json_safe_encode(array(
			'summary'=>array(
				'_flang'=>$flang,
				'raw'=>$vData['description'],
				'processed'=>'<p>' . $vData['description'] . '</p>'
				)
			));
		$data['episodeStatus'] = 'Published';
		if (is_object($pShow)) {
			$default_url = $pShow->get_option('statsURL');
		}
		$data['episodeTrackedURL'] = preg_replace_callback('/{([A-Za-z0-9_\-]+)}/', function($matches) use ($data, $pShow){
                if ($matches[1] == 'showSlug') {
                	return $pShow->showSlug();
                }
                if ($matches[1] == 'episodeNumber') {
                	return $data['episodeNumber'];
                }
            }, $default_url);
		var_dump($data);
		$newshow = $pEpisodeFactory->create($data);
		if (is_object($newshow)) {
			$vEpisodeData = array();
			$vEpisodeData['episodeID'] = $newshow->id();
			$vEpisodeData['vimeoURI'] = $vData['uri'];
			var_dump($vEpisodeData);
			return $this->create($vEpisodeData);
		}
		return null;
	}
}